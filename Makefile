IMG ?= default-route-openshift-image-registry.apps-crc.testing/webhook-example/hcm:latest

.PHONY: clean push build

push: build
	@podman push $(IMG) --tls-verify=false

build:
	@podman build --rm -t $(IMG) .
	@pushd deploy; kustomize edit set image webhook-example=$(IMG); popd

