FROM registry.access.redhat.com/ubi8/python-39 AS builder
USER root
WORKDIR /build
COPY src /build
RUN pip3 install -r requirements.txt
RUN pyinstaller -s -F webhook-example.py --hidden-import=gunicorn.glogging --hidden-import=gunicorn.workers.sync
RUN mkdir -p /target/hcs-company.com
RUN mkdir -m 1777 /target/tmp
RUN staticx /build/dist/webhook-example /target/hcs-company.com/webhook-example; chmod 755 /target/hcs-company.com/webhook-example


FROM scratch
WORKDIR /
USER root
COPY --from=builder /target /
CMD ["/hcs-company.com/webhook-example"]
USER 1001
