import base64
import copy
import gunicorn.app.base
import http
import json
import jsonpatch
import logging
import os
import random
from flask import Flask, jsonify, request
from six import iteritems

allowed_loglevels = [
  'debug',
  'info',
  'warning',
  'error',
  'critical'
]


app = Flask(__name__)

@app.route('/mutate', methods=['POST'])
def mutating_webhook():
  app.logger.debug(f'Request JSON: {request.json}')
  orig = request.json["request"]["object"]
  modified = copy.deepcopy(orig)
  namespace = request.json["request"]["name"]
  namespace_prefix = namespace.split('-')[0]
  if 'labels' not in modified["metadata"]:
    modified["metadata"]["labels"] = {}
  if 'quotagroup' not in modified["metadata"]["labels"]:
    modified["metadata"]["labels"]["quotagroup"] = namespace_prefix
    app.logger.info(f'Patching namespace {namespace} with label "quotagroup: {namespace_prefix}"')
  patch = jsonpatch.JsonPatch.from_diff(orig, modified) 
  response = {
    'apiVersion': 'admission.k8s.io/v1',
    'kind': 'AdmissionReview',
    'response': {
      "allowed": True,
      "uid": request.json["request"]["uid"],
      "patch": base64.b64encode(str(patch).encode()).decode(),
      "patchType": "JSONPatch"
    }
  }
  app.logger.debug(f'Response JSON: {response}')
  return response

class GunicornApp(gunicorn.app.base.BaseApplication):
  def __init__(self, app, options):
    self.options = options or {}
    self.application = app
    super(GunicornApp, self).__init__()

  def load_config(self):
    config = dict([(key, value) for key, value in iteritems(self.options)
                   if key in self.cfg.settings and value is not None])
    for key, value in iteritems(config):
      self.cfg.set(key.lower(), value)

  def load(self):
    gunicorn_logger = logging.getLogger('gunicorn.error')
    self.application.logger.handlers = gunicorn_logger.handlers
    self.application.logger.setLevel(gunicorn_logger.level)
    return self.application

if __name__ == "__main__":
  loglevel = os.getenv('LOGLEVEL', 'INFO').lower()
  if loglevel  not in allowed_loglevels:
    loglevel = 'info'

  certfile = os.getenv('CERTFILE', '/certs/tls.crt')
  keyfile = os.getenv('KEYFILE', '/certs/tls.key')

  options = {
    'bind': '0.0.0.0:8443',
    'certfile': certfile,
    'keyfile': keyfile,
    'loglevel': loglevel,
    'workers': 1
  }
  GA = GunicornApp(app, options)
  GA.run()
