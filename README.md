Kubernetes Mutating WebHook Admission Controller
================================================

This repository contains an example of how to create and configure a Mutating
Webhook Admission Controller for Kubernetes in Python. It uses some OpenShift
specific annotations to get a serving certificate in a secret, and to inject a
trusted CABundle on the MutatingWebhookConfiguration. If you do not run
OpenShift but another flavor of Kubernetes adjust to match your certificate
management solution (and change the `oc` commands below to `kubectl`).

## Index

<!-- vim-markdown-toc GitLab -->

* [Video Explanation](#video-explanation)
* [Quick Admission Flow Overview](#quick-admission-flow-overview)
* [Pre-Built Usage](#pre-built-usage)
* [Build It Yourself](#build-it-yourself)
* [Using a Different Image](#using-a-different-image)
* [Deploying Your Own Image](#deploying-your-own-image)
* [Cleanup](#cleanup)

<!-- vim-markdown-toc -->

## Video Explanation

This repository was used for the HCS Company Sudo Friday video on February 18th
2022. You can view the video
[here](https://www.youtube.com/watch?v=nxcxvCvzk3U). Please note that the video
is in Dutch.

[![](https://img.youtube.com/vi/nxcxvCvzk3U/0.jpg)](https://www.youtube.com/watch?v=nxcxvCvzk3U "HCS Sudo Friday - Mutating Webhook Admission Controllers")

The video contains more explanation on the organization of the code, the setup
of the `MutatingWebhookConfiguration` and more.


## Quick Admission Flow Overview

Whenever a request comes in to a Kubernetes API Server it follows the following flow:

```mermaid
graph LR;
R(Request)-->A(API Server);
A-->RBAC;
RBAC-->MA(Mutating Admission Controller);
MA-->WH1(Webhook Servers);
WH1-->MA
MA-->SV(Schema Validation);
SV-->VA(Validating Admission Controller);
VA-->WH2(Webhook Servers);
WH2-->VA
VA-->ETCD(ETCD Persistence);

```

## Pre-Built Usage
If you just want to use a pre-built image run the following command:
```bash
oc apply -k deploy
```

## Build It Yourself

This repo comes with a `Makefile` that can build and push the image using
`podman`.  It is configured by default to push to a local CodeReadyContainers
(CRC) cluster, running on default hostnames and ports. In order to push to the
registry on CRC you must first login to the registry:

```bash
oc login -u <username> https://<crc-api-address>
podman login -u <username> -p $(oc whoami -t)  default-route-openshift-image-registry.apps-crc.testing --tls-verify=false
```

Then build and push the image:

```bash
make
```

## Using a Different Image

If you want to push to a different image you can set the `IMG` environment
variable for `make`, for example, to use the
`registry.example.com:5000/examplens/exampleimg:latest` image:

```bash
IMG=registry.example.com:5000/examplens/exampleimg:latest make
```

You can also update the `IMG` variable in the `Makefile` to make this change permanent.

## Deploying Your Own Image

The `build` step in the `Makefile` automatically updates the
`deploy/kustomization.yaml` file to reference your custom image. This means the
deployment instructions are the same as for the pre-built image.

## Cleanup
In order to clean up after usage you can use the following command:

```bash
oc delete -k deploy
```

This will clean up both after a pre-built deploy or after a custom iamge deploy.
